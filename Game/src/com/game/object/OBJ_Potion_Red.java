package com.game.object;

import com.game.entity.Entity;
import com.game.main.GamePanel;

public class OBJ_Potion_Red extends Entity {


    public OBJ_Potion_Red(GamePanel gamePanel) {
        super(gamePanel);
        value = 5;
        type = typeConsumable;
        name = "Red Potion";
        down1 = setup("/objects/potion_red", gamePanel.tileSize, gamePanel.tileSize);
        itemDescription = "[" + name + "]\nA red potion.\nHeals you by " + value + " health";
    }

    public void use(Entity entity) {
        gamePanel.gameState = gamePanel.dialogState;
        gamePanel.ui.currentDialogue = "You drink the " + name + "!\nYour life have been recovered by " + value + ".";
        entity.addLife(value);
        gamePanel.playSE(2);
    }
}
