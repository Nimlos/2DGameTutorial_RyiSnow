package com.game.object;

import com.game.entity.Entity;
import com.game.main.GamePanel;

public class OBJ_Shield_Wood extends Entity {
    public OBJ_Shield_Wood(GamePanel gamePanel) {
        super(gamePanel);
        name = "Wooden Shield";
        down1 = setup("/objects/shield_wood", gamePanel.tileSize, gamePanel.tileSize);
        defenceValue = 1;
        itemDescription = "[" + name + "]\nMade by wood.";
        type = typeShield;
    }
}
