package com.game.object;

import com.game.entity.Entity;
import com.game.main.GamePanel;

public class OBJ_Heart extends Entity {

    public OBJ_Heart(GamePanel gamePanel) {
        super(gamePanel);

        type = typePickupOnly;
        name = "Heart";
        value = 2;
        down1 = setup("/objects/heart_full", gamePanel.tileSize, gamePanel.tileSize);
        image = setup("/objects/heart_full", gamePanel.tileSize, gamePanel.tileSize);
        image2 = setup("/objects/heart_half", gamePanel.tileSize, gamePanel.tileSize);
        image3 = setup("/objects/heart_blank", gamePanel.tileSize, gamePanel.tileSize);

    }

    public void use(Entity user) {
        gamePanel.playSE(2);
        gamePanel.ui.addMessage("Life + " + value);
        user.addLife(value);

    }
}
