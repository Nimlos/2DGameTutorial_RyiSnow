package com.game.object;

import com.game.entity.Entity;
import com.game.main.GamePanel;

public class OBJ_ManaCrystal extends Entity {
    public OBJ_ManaCrystal(GamePanel gamePanel) {
        super(gamePanel);

        type = typePickupOnly;
        name = "Mana Crystal";
        value = 1;
        down1 = setup("/objects/manacrystal_full", gamePanel.tileSize, gamePanel.tileSize);
        image = setup("/objects/manacrystal_full", gamePanel.tileSize, gamePanel.tileSize);

        image2 = setup("/objects/manacrystal_blank", gamePanel.tileSize, gamePanel.tileSize);
    }

    public void use(Entity entity) {
        gamePanel.ui.addMessage("Mana + " + value);
        entity.addMana(value);
        gamePanel.playSE(2);
    }


}
