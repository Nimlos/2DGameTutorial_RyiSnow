package com.game.object;

import com.game.entity.Entity;
import com.game.main.GamePanel;

import javax.imageio.ImageIO;
import java.io.IOException;

public class OBJ_Chest extends Entity {

    public OBJ_Chest(GamePanel gamePanel) {
        super(gamePanel);
        name = "Chest";
        down1 = setup("/objects/chest", gamePanel.tileSize, gamePanel.tileSize);
    }
}
