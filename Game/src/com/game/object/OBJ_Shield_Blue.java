package com.game.object;

import com.game.entity.Entity;
import com.game.main.GamePanel;

public class OBJ_Shield_Blue extends Entity {

    public OBJ_Shield_Blue(GamePanel gamePanel) {
        super(gamePanel);
        name = "Blue Shield";
        down1 = setup("/objects/shield_blue", gamePanel.tileSize, gamePanel.tileSize);
        defenceValue = 2;
        attackArea.width = 30;
        attackArea.height = 30;
        itemDescription = "[" + name + "]\nA shiny blue shield.";
        type = typeShield;
    }
}
