package com.game.object;

import com.game.entity.Entity;
import com.game.main.GamePanel;

public class OBJ_Coin_Bronze extends Entity {
    public OBJ_Coin_Bronze(GamePanel gamePanel) {
        super(gamePanel);

        type = typePickupOnly;
        name = "Bronze Coin";
        down1 = setup("/objects/coin_bronze", gamePanel.tileSize, gamePanel.tileSize);
        value = 1;
    }

    public void use(Entity entity) {
        gamePanel.playSE(1);
        gamePanel.ui.addMessage("Coin +" + value);
        gamePanel.player.coin += value;
    }
}
