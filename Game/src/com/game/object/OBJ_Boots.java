package com.game.object;

import com.game.entity.Entity;
import com.game.main.GamePanel;

public class OBJ_Boots extends Entity {

    public OBJ_Boots(GamePanel gamePanel) {
        super(gamePanel);
        name = "Boots";
        down1 = setup("/objects/boots", gamePanel.tileSize, gamePanel.tileSize);

    }
}
