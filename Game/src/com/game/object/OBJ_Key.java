package com.game.object;

import com.game.entity.Entity;
import com.game.main.GamePanel;

public class OBJ_Key extends Entity {

    public OBJ_Key(GamePanel gamePanel) {
        super(gamePanel);

        name = "Key";
        down1 = setup("/objects/key", gamePanel.tileSize, gamePanel.tileSize);
        itemDescription = "[" + name + "]\nOpen the door.";
    }
}
