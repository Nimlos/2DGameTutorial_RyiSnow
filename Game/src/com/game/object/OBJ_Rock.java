package com.game.object;

import com.game.entity.Entity;
import com.game.entity.Projectile;
import com.game.main.GamePanel;

public class OBJ_Rock extends Projectile {
    public OBJ_Rock(GamePanel gamePanel) {
        super(gamePanel);

        name = "Rock";
        speed = 8;
        maxLife = 80;
        life = maxLife;

        attack = 2;
        useCost = 1;
        alive = false;

        getImage();
    }

    public void getImage() {
        up1 = setup("/projectile/rock_down_1", gamePanel.tileSize, gamePanel.tileSize);
        up2 = setup("/projectile/rock_down_1", gamePanel.tileSize, gamePanel.tileSize);
        down1 = setup("/projectile/rock_down_1", gamePanel.tileSize, gamePanel.tileSize);
        down2 = setup("/projectile/rock_down_1", gamePanel.tileSize, gamePanel.tileSize);
        left1 = setup("/projectile/rock_down_1", gamePanel.tileSize, gamePanel.tileSize);
        left2 = setup("/projectile/rock_down_1", gamePanel.tileSize, gamePanel.tileSize);
        right1 = setup("/projectile/rock_down_1", gamePanel.tileSize, gamePanel.tileSize);
        right2 = setup("/projectile/rock_down_1", gamePanel.tileSize, gamePanel.tileSize);
    }

    public boolean haveResource(Entity user) {
        boolean haveResource = false;
        if(user.ammo >= useCost) {
            haveResource = true;
        }
        return haveResource;
    }

    public void subtractResource(Entity user) {
        user.ammo -= useCost;
    }
}
