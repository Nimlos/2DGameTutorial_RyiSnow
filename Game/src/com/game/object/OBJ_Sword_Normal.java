package com.game.object;

import com.game.entity.Entity;
import com.game.main.GamePanel;

public class OBJ_Sword_Normal extends Entity {
    public OBJ_Sword_Normal(GamePanel gamePanel) {
        super(gamePanel);

        name = "Normal Sword";
        down1 = setup("/objects/sword_normal", gamePanel.tileSize, gamePanel.tileSize);
        attackValue = 1;
        itemDescription = "[" + name + "]\nAn old sword.";
        attackArea.width = 36;
        attackArea.height = 36;
        type = typeSword;
    }


}
