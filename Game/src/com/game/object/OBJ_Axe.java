package com.game.object;

import com.game.entity.Entity;
import com.game.main.GamePanel;

public class OBJ_Axe extends Entity {
    public OBJ_Axe(GamePanel gamePanel) {
        super(gamePanel);
        name = "Woodcutter's Axe";
        down1 = setup("/objects/axe", gamePanel.tileSize, gamePanel.tileSize);
        attackValue = 2;
        attackArea.width = 30;
        attackArea.height = 30;
        itemDescription = "[" + name + "]\nA bit rusty but can\nstill cut some trees.";
        type = typeAxe;
    }
}
