package com.game.entity;

import com.game.main.GamePanel;
import com.game.main.KeyHandler;
import com.game.main.UtilityTool;
import com.game.object.*;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class Player extends Entity {

    KeyHandler keyHandler;

    public final int screenX;
    public final int screenY;
    public boolean attackCancel = false;
    public ArrayList<Entity> inventoryList = new ArrayList<>();
    public final int maxInventorySize = 20;

    public Player(GamePanel gamePanel, KeyHandler keyHandler) {
        super(gamePanel);
        this.keyHandler = keyHandler;

        screenX = gamePanel.screenWidth / 2 - (gamePanel.tileSize / 2);
        screenY = gamePanel.screenHeight / 2 - (gamePanel.tileSize / 2);

        solidArea = new Rectangle(8, 16, 32, 32);
        solidAreaDefaultX = solidArea.x;
        solidAreaDefaultY = solidArea.y;

        setDefaultValues();
        getPlayerImage();
        getPlayerAttackImage();
        setItems();
        name = "Player";
    }

    public void setDefaultValues() {

        worldX = gamePanel.tileSize * 23;
        worldY = gamePanel.tileSize * 21;

        speed = 4;
        direction = "down";

        // PLAYER STATUS
        level = 1;
        maxLife = 6;
        life = maxLife;
        strength = 1;
        dexterity = 1;
        exp = 0;
        nextLevelExp = 5;
        coin = 0;
        maxMana = 4;
        mana = maxMana;
        ammo = 10;
        currentWeapon = new OBJ_Sword_Normal(gamePanel);
        currentShield = new OBJ_Shield_Wood(gamePanel);
        projectile = new OBJ_Fireball(gamePanel);
//        projectile = new OBJ_Rock(gamePanel);
        attack = getAttack();
        defense = getDefense();
    }

    public void setItems() {
        inventoryList.add(currentWeapon);
        inventoryList.add(currentShield);
        inventoryList.add(new OBJ_Key(gamePanel));

    }

    public int getAttack() {
        attackArea = currentWeapon.attackArea;
        return strength*currentWeapon.attackValue;
    }

    public int getDefense() {
        return dexterity*currentShield.defenceValue;
    }

    public void getPlayerImage() {
        up1 = setup("/player/boy_up_1", gamePanel.tileSize, gamePanel.tileSize);
        up2 = setup("/player/boy_up_2", gamePanel.tileSize, gamePanel.tileSize);
        down1 = setup("/player/boy_down_1", gamePanel.tileSize, gamePanel.tileSize);
        down2 = setup("/player/boy_down_2", gamePanel.tileSize, gamePanel.tileSize);
        left1 = setup("/player/boy_left_1", gamePanel.tileSize, gamePanel.tileSize);
        left2 = setup("/player/boy_left_2", gamePanel.tileSize, gamePanel.tileSize);
        right1 = setup("/player/boy_right_1", gamePanel.tileSize, gamePanel.tileSize);
        right2 = setup("/player/boy_right_2", gamePanel.tileSize, gamePanel.tileSize);

    }

    public void getPlayerAttackImage() {
        if (currentWeapon.type == typeSword) {
            attackUp1 = setup("/player/boy_attack_up_1", gamePanel.tileSize, gamePanel.tileSize * 2);
            attackUp2 = setup("/player/boy_attack_up_2", gamePanel.tileSize, gamePanel.tileSize * 2);
            attackDown1 = setup("/player/boy_attack_down_1", gamePanel.tileSize, gamePanel.tileSize * 2);
            attackDown2 = setup("/player/boy_attack_down_2", gamePanel.tileSize, gamePanel.tileSize * 2);
            attackLeft1 = setup("/player/boy_attack_left_1", gamePanel.tileSize * 2, gamePanel.tileSize);
            attackLeft2 = setup("/player/boy_attack_left_2", gamePanel.tileSize * 2, gamePanel.tileSize);
            attackRight1 = setup("/player/boy_attack_right_1", gamePanel.tileSize * 2, gamePanel.tileSize);
            attackRight2 = setup("/player/boy_attack_right_2", gamePanel.tileSize * 2, gamePanel.tileSize);
        } else if(currentWeapon.type == typeAxe) {
            attackUp1 = setup("/player/boy_axe_up_1", gamePanel.tileSize, gamePanel.tileSize * 2);
            attackUp2 = setup("/player/boy_axe_up_2", gamePanel.tileSize, gamePanel.tileSize * 2);
            attackDown1 = setup("/player/boy_axe_down_1", gamePanel.tileSize, gamePanel.tileSize * 2);
            attackDown2 = setup("/player/boy_axe_down_2", gamePanel.tileSize, gamePanel.tileSize * 2);
            attackLeft1 = setup("/player/boy_axe_left_1", gamePanel.tileSize * 2, gamePanel.tileSize);
            attackLeft2 = setup("/player/boy_axe_left_2", gamePanel.tileSize * 2, gamePanel.tileSize);
            attackRight1 = setup("/player/boy_axe_right_1", gamePanel.tileSize * 2, gamePanel.tileSize);
            attackRight2 = setup("/player/boy_axe_right_2", gamePanel.tileSize * 2, gamePanel.tileSize);
        }

    }

    public void update() {

        if (attacking) {
            attacking();
        } else if (keyHandler.upPressed || keyHandler.downPressed || keyHandler.leftPressed || keyHandler.rightPressed || keyHandler.enterPressed) {
            if (keyHandler.upPressed) {
                direction = "up";
            } else if (keyHandler.downPressed) {
                direction = "down";
            } else if (keyHandler.leftPressed) {
                direction = "left";
            } else if (keyHandler.rightPressed) {
                direction = "right";
            }

            // CHECK TILE COLLISION
            collisionOn = false;
            gamePanel.collisionChecker.checkTile(this);

            // CHECK OBJECT COLLISION
            int objectIndex = gamePanel.collisionChecker.checkObject(this, true);
            pickUpObject(objectIndex);

            // CHECK NPC COLLISION
            int npcIndex = gamePanel.collisionChecker.checkEntity(this, gamePanel.npcList);
            interactNPC(npcIndex);

            // CHECK MONSTER COLLISION
            int monsterIndex = gamePanel.collisionChecker.checkEntity(this, gamePanel.monsterList);
            contactMonster(monsterIndex);


            // CHECK EVENTS
            gamePanel.eventHandler.checkEvent();

            // IF COLLISION IS FALSE PLAYER CAN MOVE
            if (!collisionOn && !keyHandler.enterPressed) {
                switch (direction) {
                    case "up":
                        worldY -= speed;
                        break;
                    case "down":
                        worldY += speed;
                        break;
                    case "left":
                        worldX -= speed;
                        break;
                    case "right":
                        worldX += speed;
                        break;
                }
            }

            if (keyHandler.enterPressed && !attackCancel) {
                gamePanel.playSE(7);
                attacking = true;
                spriteCounter = 0;
            }

            attackCancel = false;

            spriteCounter++;
            if (spriteCounter > 12) {
                if (spriteNumber == 1) {
                    spriteNumber = 2;
                } else if (spriteNumber == 2) {
                    spriteNumber = 1;
                }
                spriteCounter = 0;
            }
        }

        if (keyHandler.shotKeyPressed && !projectile.alive && shotAvailableCounter == 30
                && projectile.haveResource(this)) {
            projectile.set(worldX, worldY, direction, true, this);
            gamePanel.projectileList.add(projectile);
            gamePanel.playSE(10);
            shotAvailableCounter = 0;
            projectile.subtractResource(this);
        }

        if (invincible) {
            invincibleCounter++;
            if (invincibleCounter >= 60) {
                invincible = false;
                invincibleCounter = 0;
            }
        }

        if (shotAvailableCounter < 30) {
            shotAvailableCounter++;
        }

    }

    public void attacking() {
        spriteCounter++;
        if (spriteCounter <= 5) {
            spriteNumber = 1;
        }
        if (spriteCounter > 5 && spriteCounter <= 25) {
            spriteNumber = 2;
            checkAttack();
        }
        if (spriteCounter > 25) {
            spriteNumber = 1;
            spriteCounter = 0;
            attacking = false;
        }
    }

    private void checkAttack() {
        // SAVE THE CURRENT WORLDX WORLDY AND SOLIDAREA
        int currentWorldX = worldX;
        int currentWorldY = worldY;
        int solidAreaWidth = solidArea.width;
        int solidAreaHeight = solidArea.height;

        // ADJIST PLAYUERS WORLD X AND Y FOR ATTACK AREA
        switch (direction) {
            case "up":
                worldY -= attackArea.height;
                break;
            case "down":
                worldY += attackArea.height;
                break;
            case "left":
                worldX -= attackArea.width;
                break;
            case "right":
                worldX += attackArea.width;
                break;
        }

        // ATTACK AREA becomes solidArea
        solidArea.width = attackArea.width;
        solidArea.height = attackArea.height;

        // Check monster collision with updated values
        int monsterIndex = gamePanel.collisionChecker.checkEntity(this, gamePanel.monsterList);
        damageMonster(monsterIndex, attack);

        worldX = currentWorldX;
        worldY = currentWorldY;
        solidArea.width = solidAreaWidth;
        solidArea.height = solidAreaHeight;
    }

    public void damageMonster(int index, int attack) {
        if (index != 999) {
            if(!gamePanel.monsterList[index].invincible) {
                gamePanel.playSE(5);

                int damage = attack + gamePanel.monsterList[index].defense;
                if (damage < 0 ) {
                    damage = 0;
                }

                gamePanel.monsterList[index].life -= damage;
                gamePanel.ui.addMessage(damage + " damage!");
                gamePanel.monsterList[index].invincible = true;
                gamePanel.monsterList[index].damageReaction();

                if (gamePanel.monsterList[index].life <= 0) {
                    gamePanel.monsterList[index].dying = true;
                    gamePanel.ui.addMessage("Killed the " + gamePanel.monsterList[index].name + "!");
                    gamePanel.ui.addMessage("Exp + " + gamePanel.monsterList[index].exp);
                    exp += gamePanel.monsterList[index].exp;
                    checkLevelUp();
                }
            }
        }
    }

    public void checkLevelUp() {
        if (exp >= nextLevelExp) {
            level++;
            nextLevelExp = nextLevelExp*2;
            maxLife += 2;
            strength++;
            dexterity++;
            attack = getAttack();
            defense = getDefense();

            gamePanel.playSE(8);
            gamePanel.gameState = gamePanel.dialogState;
            gamePanel.ui.currentDialogue = "You are level " + level + " now!\n" + "you feel stronger!";
        }
    }

    public void selectItem() {
        int itemIndex = gamePanel.ui.getItemIndexOnSlot();

        if (itemIndex < inventoryList.size()) {
            Entity selectedItem = inventoryList.get(itemIndex);

            switch (selectedItem.type) {
                case typeSword:
                    currentWeapon = selectedItem;
                    attack = getAttack();
                    getPlayerAttackImage();
                    break;
                case typeAxe:
                    currentWeapon = selectedItem;
                    attack = getAttack();
                    getPlayerAttackImage();
                    break;
                case typeShield:
                    currentShield = selectedItem;
                    defense = getDefense();
                    break;
                case typeConsumable:
                    selectedItem.use(this);
                    inventoryList.remove(itemIndex);
                    break;
                default:
                    break;
            }
        }
    }

    public void pickUpObject(int index) {

        if (index != 999) {

            // PICKUP ONLY
            if (gamePanel.objectList[index].type == typePickupOnly) {

                gamePanel.objectList[index].use(this);
                gamePanel.objectList[index] = null;
            } else {
                // INVENTORY
                String text;
                if (inventoryList.size() < maxInventorySize) {
                    inventoryList.add(gamePanel.objectList[index]);
                    gamePanel.playSE(1);
                    text = "Got a " + gamePanel.objectList[index].name + "!";
                    gamePanel.objectList[index] = null;

                } else {
                    text = "You cannot carry anymore !";
                }
                gamePanel.ui.addMessage(text);
            }





        }


    }

    public void interactNPC(int index) {
        if (gamePanel.keyHandler.enterPressed) {
            if (index != 999) {
                attackCancel = true;
                gamePanel.gameState = gamePanel.dialogState;
                gamePanel.npcList[index].speak();
            }
        }

    }

    public void contactMonster(int index) {
        if (index != 999) {
            Entity monster = gamePanel.monsterList[index];

            if (!invincible && !monster.dying) {
                int damage = gamePanel.monsterList[index].attack - defense;
                if (damage < 0 ) {
                    damage = 0;
                }
                gamePanel.playSE(6);
                life -= damage;
                invincible = true;
            }
        }
    }

    public void draw(Graphics2D g2) {

        BufferedImage image = null;
        int x = screenX;
        int y = screenY;


        switch (direction) {
            case "up":
                if (!attacking) {
                    if (spriteNumber == 1) {
                        image = up1;
                    }
                    if (spriteNumber == 2) {
                        image = up2;
                    }
                } else {
                    y = screenY - gamePanel.tileSize;
                    if (spriteNumber == 1) {
                        image = attackUp1;
                    }
                    if (spriteNumber == 2) {
                        image = attackUp2;
                    }
                }
                break;
            case "down":
                if (!attacking) {
                    if (spriteNumber == 1) {
                        image = down1;
                    }
                    if (spriteNumber == 2) {
                        image = down2;
                    }
                } else {
                    if (spriteNumber == 1) {
                        image = attackDown1;
                    }
                    if (spriteNumber == 2) {
                        image = attackDown2;
                    }
                }
                break;
            case "left":
                if (!attacking) {
                    if (spriteNumber == 1) {
                        image = left1;
                    }
                    if (spriteNumber == 2) {
                        image = left2;
                    }
                } else {
                    x = screenX - gamePanel.tileSize;
                    if (spriteNumber == 1) {
                        image = attackLeft1;
                    }
                    if (spriteNumber == 2) {
                        image = attackLeft2;
                    }
                }

                break;
            case "right":
                if (!attacking) {
                    if (spriteNumber == 1) {
                        image = right1;
                    }
                    if (spriteNumber == 2) {
                        image = right2;
                    }
                } else {
                    if (spriteNumber == 1) {
                        image = attackRight1;
                    }
                    if (spriteNumber == 2) {
                        image = attackRight2;
                    }
                }

                break;
        }



        if (screenX > worldX) {
            x = worldX;
        }
        if (screenY > worldY) {
            y = worldY;
        }
        int rightOffset = gamePanel.screenWidth - screenX;
        if (rightOffset > gamePanel.worldWith - worldX) {
            x = gamePanel.screenWidth - (gamePanel.worldWith - worldX);
        }
        int bottomOffset = gamePanel.screenHeight - screenY;
        if (bottomOffset > gamePanel.worldHeight - (worldY)) {
            y = gamePanel.screenHeight - (gamePanel.worldHeight - worldY);
        }

        if (invincible) {
            g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.3f));
        }

        g2.drawImage(image, x, y, null);
        g2.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1f));

    }
}
