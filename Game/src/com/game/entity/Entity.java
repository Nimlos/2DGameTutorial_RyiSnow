package com.game.entity;

import com.game.main.GamePanel;
import com.game.main.UtilityTool;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;

public class Entity {

    public GamePanel gamePanel;
    public BufferedImage up1, up2, down1, down2, left1, left2, right1, right2;
    public BufferedImage attackUp1, attackUp2, attackDown1, attackDown2, attackLeft1, attackLeft2, attackRight1, attackRight2;
    public Rectangle solidArea = new Rectangle(0, 0, 48, 48);
    public Rectangle attackArea = new Rectangle(0, 0, 0, 0);
    public int solidAreaDefaultX, solidAreaDefaultY;
    String dialogues[] = new String[20];
    public BufferedImage image, image2, image3;
    public boolean collision = false;

    // STATE
    public int worldX, worldY;
    public String direction = "down";
    public int spriteNumber = 1;
    int dialogIndex = 0;
    public boolean collisionOn = false;
    public boolean invincible = false;
    boolean attacking = false;
    public boolean alive = true;
    public boolean dying = false;
    boolean hpBarOn = false;

    // COUNTER
    public int spriteCounter = 0;
    public int actionLockCounter = 0;
    public int invincibleCounter = 0;
    int dyingCounter = 0;
    int hpBarCounter = 0;
    public int shotAvailableCounter = 0;

    // CHARACTER ATTRIBUTES
    public String name;
    public int speed;
    public int maxLife;
    public int life;
    public int maxMana;
    public int mana;
    public int ammo;
    public int level;
    public int strength;
    public int dexterity;
    public int attack;
    public int defense;
    public int exp;
    public int nextLevelExp;
    public int coin;
    public Entity currentWeapon;
    public Entity currentShield;
    public Projectile projectile;

    // ITEM ATTRIBUTES
    public int attackValue;
    public int defenceValue;
    public String itemDescription = "";
    public int useCost;
    public int value;

    // TYPE
    public int type; // 0 = player, 1 = npc, 2 = monster
    public final int typePlayer = 0;
    public final int typeNpc = 1;
    public final int typeMonster = 2;
    public final int typeSword = 3;
    public final int typeAxe = 4;
    public final int typeShield = 5;
    public final int typeConsumable = 6;
    public final int typePickupOnly = 7;

    public Entity(GamePanel gamePanel) {
        this.gamePanel = gamePanel;
    }

    public BufferedImage setup(String imageName, int width, int height) {
        UtilityTool tool = new UtilityTool();
        BufferedImage scaledImage = null;
        try {
            scaledImage = ImageIO.read(getClass().getResourceAsStream(imageName + ".png"));
            scaledImage = tool.scaleImage(scaledImage, width, height);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return scaledImage;
    }

    public void setAction() {
    }

    public void damageReaction() {

    }

    public void speak() {
        if (dialogues[dialogIndex] == null) {
            dialogIndex = 0;
        }
        gamePanel.ui.currentDialogue = dialogues[dialogIndex];
        dialogIndex++;

        switch (gamePanel.player.direction) {
            case "up":
                direction = "down";
                break;
            case "down":
                direction = "up";
                break;
            case "left":
                direction = "right";
                break;
            case "right":
                direction = "left";
                break;
        }
    }

    public void use(Entity entity) {
    }

    public void checkDrop() {}

    public void dropItem(Entity droppedItem) {
        for (int i = 0; i < gamePanel.objectList.length; i++) {
            if (gamePanel.objectList[i] == null) {
                gamePanel.objectList[i] = droppedItem;
                gamePanel.objectList[i].worldX = worldX; // The dead monster's worldX
                gamePanel.objectList[i].worldY = worldY;
                break;
            }
        }
    }

    public void update() {

        setAction();

        collisionOn = false;
        gamePanel.collisionChecker.checkTile(this);
        gamePanel.collisionChecker.checkObject(this, false);
        gamePanel.collisionChecker.checkEntity(this, gamePanel.npcList);
        gamePanel.collisionChecker.checkEntity(this, gamePanel.monsterList);
        boolean contactPlayer = gamePanel.collisionChecker.checkPlayer(this);

        if (this.type == typeMonster && contactPlayer) {
            damagePlayer(attack);
        }

        if (!collisionOn) {
            switch (direction) {
                case "up":
                    worldY -= speed;
                    break;
                case "down":
                    worldY += speed;
                    break;
                case "left":
                    worldX -= speed;
                    break;
                case "right":
                    worldX += speed;
                    break;
            }
        }

        spriteCounter++;
        if (spriteCounter > 12) {
            if (spriteNumber == 1) {
                spriteNumber = 2;
            } else if (spriteNumber == 2) {
                spriteNumber = 1;
            }
            spriteCounter = 0;
        }

        if (invincible) {
            invincibleCounter++;
            if (invincibleCounter >= 40) {
                invincible = false;
                invincibleCounter = 0;
            }
        }

        if (shotAvailableCounter < 30) {
            shotAvailableCounter++;
        }
    }

    public void damagePlayer(int attack) {
        if (!gamePanel.player.invincible) {

            int damage = attack - gamePanel.player.defense;
            if (damage < 0) {
                damage = 0;
            }
            gamePanel.playSE(6);
            gamePanel.player.life -= damage;
            gamePanel.player.invincible = true;
        }
    }

    public void dyingAnimation(Graphics2D graphics2D) {
        dyingCounter++;

        int i = 5;

        if (dyingCounter <= i) changeAlpha(graphics2D, 0f);
        if (dyingCounter > i && dyingCounter <= i * 2) changeAlpha(graphics2D, 1f);
        if (dyingCounter > i * 2 && dyingCounter <= i * 3) changeAlpha(graphics2D, 0f);
        if (dyingCounter > i * 3 && dyingCounter <= i * 4) changeAlpha(graphics2D, 1f);
        if (dyingCounter > i * 4 && dyingCounter <= i * 5) changeAlpha(graphics2D, 0f);
        if (dyingCounter > i * 5 && dyingCounter <= i * 6) changeAlpha(graphics2D, 1f);
        if (dyingCounter > i * 6 && dyingCounter <= i * 7) changeAlpha(graphics2D, 0f);
        if (dyingCounter > i * 7 && dyingCounter <= i * 8) changeAlpha(graphics2D, 1f);

        if (dyingCounter > i * 8) {
            dyingCounter = 0;
            alive = false;
            dying = false;
        }


    }

    public void changeAlpha(Graphics2D graphics2D, float alpha) {
        graphics2D.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha));
    }

    public void addLife(int value) {
        life += value;
        if (life > maxLife) {
            life = maxLife;
        }
    }

    public void addMana(int value) {
        mana += value;
        if (mana > maxMana) {
            mana = maxMana;
        }
    }

    public void draw(Graphics2D g2) {
        BufferedImage image = null;
        int screenX = worldX - gamePanel.player.worldX + gamePanel.player.screenX;
        int screenY = worldY - gamePanel.player.worldY + gamePanel.player.screenY;

        if (worldX + gamePanel.tileSize > gamePanel.player.worldX - gamePanel.player.screenX
                && worldX - gamePanel.tileSize < gamePanel.player.worldX + gamePanel.player.screenX
                && worldY + gamePanel.tileSize > gamePanel.player.worldY - gamePanel.player.screenY
                && worldY - gamePanel.tileSize < gamePanel.player.worldY + gamePanel.player.screenY) {

            switch (direction) {
                case "up":
                    if (spriteNumber == 1) {
                        image = up1;
                    }
                    if (spriteNumber == 2) {
                        image = up2;
                    }

                    break;
                case "down":
                    if (spriteNumber == 1) {
                        image = down1;
                    }
                    if (spriteNumber == 2) {
                        image = down2;
                    }
                    break;
                case "left":
                    if (spriteNumber == 1) {
                        image = left1;
                    }
                    if (spriteNumber == 2) {
                        image = left2;
                    }
                    break;
                case "right":
                    if (spriteNumber == 1) {
                        image = right1;
                    }
                    if (spriteNumber == 2) {
                        image = right2;
                    }
                    break;
            }

            // MONSTER HEAÆTJ NAR
            if (type == typeMonster && hpBarOn) {

                double oneScale = (double) gamePanel.tileSize / maxLife;
                double hpBarValue = oneScale * life;

                g2.setColor(new Color(35, 35, 35));
                g2.fillRect(screenX - 1, screenY - 16, gamePanel.tileSize + 2, 12);

                g2.setColor(new Color(255, 0, 30));
                g2.fillRect(screenX, screenY - 15, (int) hpBarValue, 10);

                hpBarCounter++;

                if (hpBarCounter > 600) {
                    hpBarCounter = 0;
                    hpBarOn = false;
                }
            }


            if (invincible) {
                hpBarOn = true;
                hpBarCounter = 0;
                changeAlpha(g2, 0.4f);
            }

            if (dying) {
                dyingAnimation(g2);
            }

            g2.drawImage(image, screenX, screenY,null);
            changeAlpha(g2, 1f);
        }
    }

}
