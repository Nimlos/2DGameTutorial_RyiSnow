package com.game.main;

import com.game.entity.Entity;
import com.game.entity.Player;
import com.game.tile.TileManager;

import javax.swing.*;
import java.awt.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class GamePanel extends JPanel implements Runnable {

    // SCREEN SETTINGS
    final int originalTileSize = 16; // 16x16 tile
    final int scale = 3;

    public final int tileSize = originalTileSize * scale;  // 48x48 tile
    public final int maxScreenCol = 16;
    public final int maxScreenRow = 12;
    public final int screenWidth = tileSize * maxScreenCol; // 768 pixels
    public final int screenHeight = tileSize * maxScreenRow; // 576 pixels

    // WORLD SETTINGS
    public final int maxWorldCol = 50;
    public final int maxWorldRow = 50;
    public final int worldWith = tileSize * maxWorldCol;
    public final int worldHeight = tileSize * maxWorldRow;

    // FPS
    int FPS = 60;

    // SYSTEM
    TileManager tileManager = new TileManager(this);
    public KeyHandler keyHandler = new KeyHandler(this);
    Sound music = new Sound();
    Sound soundEffect = new Sound();
    public CollisionChecker collisionChecker = new CollisionChecker(this);
    public AssetSetter assetSetter = new AssetSetter(this);
    public UI ui = new UI(this);
    Thread gameThread;
    public EventHandler eventHandler = new EventHandler(this);

    // ENTITY AND OBJECTS
    public Player player = new Player(this, keyHandler);
    public Entity objectList[] = new Entity[10];
    public Entity npcList[] = new Entity[10];
    public Entity monsterList[] = new Entity[20];
    ArrayList<Entity> entityList = new ArrayList<>();
    public ArrayList<Entity> projectileList = new ArrayList<>();

    // DEBUG
    long drawStart = 0;
    long drawEnd;
    long passed;
    DecimalFormat debuggerFormat = new DecimalFormat("#0.000");
    int counterDebug = 0;
    float passedTotal;
    String debugText = "N/A";
    String debugPercentText = "N/A";
    double percentFrame;
    final double frameTimeMs = 1000/60;

    // GAME STATE
    public int gameState;
    public final int titleState = 0;
    public final int playState = 1;
    public final int pauseState = 2;
    public final int dialogState = 3;
    public final int characterState = 4;


    public GamePanel() {
        this.setPreferredSize(new Dimension(screenWidth, screenHeight));
        this.setBackground(Color.BLACK);
        this.setDoubleBuffered(true);
        this.addKeyListener(keyHandler);
        this.setFocusable(true);
    }

    public void setupGame() {
        assetSetter.setObject();
        assetSetter.setNPC();
        assetSetter.setMonster();
        gameState = titleState;
    }

    public void startGameThread() {

        gameThread = new Thread(this);
        gameThread.start();
    }

    @Override
    public void run() {

        double drawInterval = 1000000000 / (double) FPS;
        double delta = 0;
        long lastTime = System.nanoTime();
        long currentTime;
        long timer = 0;
        int drawCount = 0;

        while (gameThread != null) {

            currentTime = System.nanoTime();

            delta += (currentTime - lastTime) / drawInterval;
            timer += (currentTime - lastTime);

            lastTime = currentTime;

            if (delta >= 1) {
                update();
                repaint();
                delta--;
                drawCount++;
            }

            if (timer >= 1000000000) {
                if (drawCount < FPS) {
                    System.out.println("FPS: " + drawCount);
                }
                drawCount = 0;
                timer = 0;
            }
        }

    }

    public void update() {
        if (gameState == playState) {
            player.update();

            // NPC
            for (Entity entity : npcList) {
                if (entity != null) {
                    entity.update();
                }
            }

            // MONSTER

            for (int i = 0; i < monsterList.length; i++) {
                if (monsterList[i] != null && !monsterList[i].dying) {
                    if(monsterList[i].alive) {
                        monsterList[i].update();
                    } else if (!monsterList[i].alive){
                        monsterList[i].checkDrop();

                        monsterList[i] = null;
                    }
                }
            }

            // PROJECTILES

            for (int i = 0; i < projectileList.size(); i++) {
                if (projectileList.get(i) != null) {
                    if(projectileList.get(i).alive) {
                        projectileList.get(i).update();
                    } else if (!projectileList.get(i).alive){
                        projectileList.remove(i);
                    }
                }
            }
        }
        if (gameState == pauseState) {
            // WIP
        }
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        Graphics2D g2 = (Graphics2D) g;

        // DEBUG
        if (keyHandler.showDebug) drawStart = System.nanoTime();

        // TITLE SCREEN
        if (gameState == titleState) {
            ui.draw(g2);
        }

        // OTHERS
        else {
            // TILE
            tileManager.draw(g2);

            // ADD ENTITIES TO THE LIST
            entityList.add(player);

            for (Entity npc : npcList) {
                if (npc != null) {
                    entityList.add(npc);
                }
            }

            for (Entity object : objectList) {
                if (object != null) {
                    entityList.add(object);
                }
            }

            for (Entity monster : monsterList) {
                if (monster != null) {
                    entityList.add(monster);
                }
            }

            for (Entity projectile : projectileList) {
                if (projectile != null) {
                    entityList.add(projectile);
                }
            }

            // SORT
            Collections.sort(entityList, new Comparator<Entity>() {
                @Override
                public int compare(Entity e1, Entity e2) {
                    int result = Integer.compare(e1.worldY, e2.worldY);
                    return result;
                }
            });

            // DRAW ENTITIES
            for (Entity entity : entityList) {
                entity.draw(g2);
            }


            // EMPTY LIST
            entityList.clear();

            // UI
            ui.draw(g2);
        }


        // DEBUG

        if (keyHandler.showDebug) {
            drawEnd = System.nanoTime();

            passed = (drawEnd - drawStart);
            passedTotal += passed;
            counterDebug++;

            if (counterDebug >= 60) {
                passedTotal = passedTotal / counterDebug;
                passedTotal /= 1000000;
                debugText = "Draw mean time 1s: \t" + debuggerFormat.format(passedTotal) + "\t ms";

                percentFrame = ((frameTimeMs - passedTotal)/frameTimeMs)*100;
                debugPercentText = "Free draw time: " + debuggerFormat.format(percentFrame) + "%";

                counterDebug = 0;
                passedTotal = 0;
            }

            g2.setFont(new Font("Arial", Font.PLAIN, 20));
            g2.setColor(Color.WHITE);
            int x = 10;
            int y = 400;
            int lineHeight = 20;

            g2.setColor(Color.BLACK);
            g2.drawString("WorldX: " + player.worldX, x+2, y+2);
            g2.setColor(Color.WHITE);
            g2.drawString("WorldX: " + player.worldX, x, y);
            y += lineHeight;
            g2.setColor(Color.BLACK);
            g2.drawString("WorldY: " + player.worldY, x+2, y+2);
            g2.setColor(Color.WHITE);
            g2.drawString("WorldY: " + player.worldY, x, y);
            y += lineHeight;
            g2.setColor(Color.BLACK);
            g2.drawString("Col: " + (player.worldX + player.solidArea.x)/tileSize, x+2, y+2);
            g2.setColor(Color.WHITE);
            g2.drawString("Col: " + (player.worldX + player.solidArea.x)/tileSize, x, y);
            y += lineHeight;
            g2.setColor(Color.BLACK);
            g2.drawString("Row: " + (player.worldY + player.solidArea.y)/tileSize, x+2, y+2);
            g2.setColor(Color.WHITE);
            g2.drawString("Row: " + (player.worldY + player.solidArea.y)/tileSize, x, y);
            y += lineHeight;
            g2.setColor(Color.BLACK);
            g2.drawString(debugText, x+2, y+2);
            g2.setColor(Color.WHITE);
            g2.drawString(debugText, x, y);
            y += lineHeight;
            g2.setColor(Color.BLACK);
            g2.drawString(debugPercentText, x+2, y+2);
            g2.setColor(Color.WHITE);
            g2.drawString(debugPercentText, x, y);
        }


        g.dispose();
    }

    public void playMusic(int i) {
        music.setFile(i);
        music.play();
        music.loop();
    }

    public void stopMusic() {
        music.stop();
    }

    public void playSE(int i) {
        soundEffect.setFile(i);
        soundEffect.play();
    }
}
