package com.game.main;

import com.game.entity.NPC_OldMan;
import com.game.monster.MON_GreenSlime;
import com.game.object.*;

public class AssetSetter {

    GamePanel gamePanel;

    public AssetSetter(GamePanel gamePanel) {
        this.gamePanel = gamePanel;
    }

    public void setObject() {
        int i = 0;
        gamePanel.objectList[i] = new OBJ_Coin_Bronze(gamePanel);
        gamePanel.objectList[i].worldX = gamePanel.tileSize*25;
        gamePanel.objectList[i].worldY = gamePanel.tileSize*19;
        i++;

        gamePanel.objectList[i] = new OBJ_Coin_Bronze(gamePanel);
        gamePanel.objectList[i].worldX = gamePanel.tileSize*21;
        gamePanel.objectList[i].worldY = gamePanel.tileSize*19;
        i++;

        gamePanel.objectList[i] = new OBJ_Axe(gamePanel);
        gamePanel.objectList[i].worldX = gamePanel.tileSize*33;
        gamePanel.objectList[i].worldY = gamePanel.tileSize*21;
        i++;

        gamePanel.objectList[i] = new OBJ_Shield_Blue(gamePanel);
        gamePanel.objectList[i].worldX = gamePanel.tileSize*35;
        gamePanel.objectList[i].worldY = gamePanel.tileSize*21;
        i++;

        gamePanel.objectList[i] = new OBJ_Potion_Red(gamePanel);
        gamePanel.objectList[i].worldX = gamePanel.tileSize*22;
        gamePanel.objectList[i].worldY = gamePanel.tileSize*27;
        i++;

        gamePanel.objectList[i] = new OBJ_Heart(gamePanel);
        gamePanel.objectList[i].worldX = gamePanel.tileSize*22;
        gamePanel.objectList[i].worldY = gamePanel.tileSize*29;
        i++;

        gamePanel.objectList[i] = new OBJ_ManaCrystal(gamePanel);
        gamePanel.objectList[i].worldX = gamePanel.tileSize*22;
        gamePanel.objectList[i].worldY = gamePanel.tileSize*31;
        i++;
    }

    public void setNPC() {

        gamePanel.npcList[0] = new NPC_OldMan(gamePanel);
        gamePanel.npcList[0].worldX = gamePanel.tileSize*21;
        gamePanel.npcList[0].worldY = gamePanel.tileSize*21;

    }

    public void setMonster() {
        int i = 0;
        gamePanel.monsterList[i] = new MON_GreenSlime(gamePanel);
        gamePanel.monsterList[i].worldX =  gamePanel.tileSize*23;
        gamePanel.monsterList[i].worldY =  gamePanel.tileSize*36;

        i++;
        gamePanel.monsterList[i] = new MON_GreenSlime(gamePanel);
        gamePanel.monsterList[i].worldX =  gamePanel.tileSize*23;
        gamePanel.monsterList[i].worldY =  gamePanel.tileSize*37;

        i++;
        gamePanel.monsterList[i] = new MON_GreenSlime(gamePanel);
        gamePanel.monsterList[i].worldX =  gamePanel.tileSize*24;
        gamePanel.monsterList[i].worldY =  gamePanel.tileSize*37;

        i++;
        gamePanel.monsterList[i] = new MON_GreenSlime(gamePanel);
        gamePanel.monsterList[i].worldX =  gamePanel.tileSize*34;
        gamePanel.monsterList[i].worldY =  gamePanel.tileSize*42;

        i++;
        gamePanel.monsterList[i] = new MON_GreenSlime(gamePanel);
        gamePanel.monsterList[i].worldX =  gamePanel.tileSize*38;
        gamePanel.monsterList[i].worldY =  gamePanel.tileSize*42;

    }
}
