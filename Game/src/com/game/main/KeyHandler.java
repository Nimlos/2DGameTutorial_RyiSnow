package com.game.main;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class KeyHandler implements KeyListener {

    public boolean upPressed, downPressed, leftPressed, rightPressed, enterPressed, shotKeyPressed;
    GamePanel gamePanel;

    public KeyHandler(GamePanel gamePanel) {
        this.gamePanel = gamePanel;
    }

    //DEBUG
    boolean showDebug = false;

    @Override
    public void keyTyped(KeyEvent keyEvent) {

    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
        int code = keyEvent.getKeyCode();

        // TITLE STATE
        if (gamePanel.gameState == gamePanel.titleState) titleState(code);

        // PLAY STATE
        else if (gamePanel.gameState == gamePanel.playState) playState(code);

        // PAUSE STATE
        else if (gamePanel.gameState == gamePanel.pauseState) pauseState(code);

        // DIALOG STATE
        else if (gamePanel.gameState == gamePanel.dialogState) dialogState(code);

        // CHARACTER STATE
        else if (gamePanel.gameState == gamePanel.characterState) characterState(code);

    }

    public void titleState(int code) {
        if (code == KeyEvent.VK_W) {
            gamePanel.ui.commandNumber--;
            if (gamePanel.ui.commandNumber < 0) {
                gamePanel.ui.commandNumber = 2;
            }
        }
        if (code == KeyEvent.VK_S) {
            gamePanel.ui.commandNumber++;
            if (gamePanel.ui.commandNumber > 2) {
                gamePanel.ui.commandNumber = 0;
            }
        }
        if (code == KeyEvent.VK_ENTER) {
            if (gamePanel.ui.commandNumber == 0) {
                gamePanel.gameState = gamePanel.playState;
                //gamePanel.playMusic(0);
            }
            if (gamePanel.ui.commandNumber == 1) {
                // LOAD GAME ADD LATER
            }
            if (gamePanel.ui.commandNumber == 2) {
                System.exit(0);
            }
        }
    }

    public void playState(int code) {
        // PLAYER CONTROL
        if (code == KeyEvent.VK_W) {
            upPressed = true;
        }
        if (code == KeyEvent.VK_S) {
            downPressed = true;
        }
        if (code == KeyEvent.VK_A) {
            leftPressed = true;
        }
        if (code == KeyEvent.VK_D) {
            rightPressed = true;
        }
        if (code == KeyEvent.VK_C) {
            gamePanel.gameState = gamePanel.characterState;
        }
        if (code == KeyEvent.VK_ENTER) {
            enterPressed = true;
        }
        if (code == KeyEvent.VK_F) {
            shotKeyPressed = true;
        }

        // PAUSE
        if (code == KeyEvent.VK_P) {
            gamePanel.gameState = gamePanel.pauseState;
        }
        // DEBUG
        if (code == KeyEvent.VK_T) {
            if (!showDebug) {
                showDebug = true;
            } else if (showDebug) {
                showDebug = false;
            }
        }

        if (code == KeyEvent.VK_R) {
            gamePanel.tileManager.loadMap("/maps/worldV2.txt");
        }
    }

    public void pauseState(int code) {
        if (code == KeyEvent.VK_P) {
            gamePanel.gameState = gamePanel.playState;
        }
    }

    public void dialogState(int code) {
        if (code == KeyEvent.VK_ENTER) {
            gamePanel.gameState = gamePanel.playState;
        }
    }

    public void characterState(int code) {
        if (code == KeyEvent.VK_C) {
            gamePanel.gameState = gamePanel.playState;
        }
        if (code == KeyEvent.VK_W) {
            if (gamePanel.ui.slotRow > 0) {
                gamePanel.ui.slotRow --;
                gamePanel.playSE(9);
            }

        }
        if (code == KeyEvent.VK_A) {
            if (gamePanel.ui.slotCol > 0) {
                gamePanel.ui.slotCol--;
                gamePanel.playSE(9);
            }

        }
        if (code == KeyEvent.VK_S) {
            if (gamePanel.ui.slotRow < 3) {
                gamePanel.ui.slotRow ++;
                gamePanel.playSE(9);
            }
        }
        if (code == KeyEvent.VK_D) {
            if (gamePanel.ui.slotCol < 4) {
                gamePanel.ui.slotCol++;
                gamePanel.playSE(9);
            }
        }
        if (code == KeyEvent.VK_ENTER) {
            System.out.println("Her");
            gamePanel.player.selectItem();
        }
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {
        int code = keyEvent.getKeyCode();

        if (code == KeyEvent.VK_W) {
            upPressed = false;
        }
        if (code == KeyEvent.VK_S) {
            downPressed = false;
        }
        if (code == KeyEvent.VK_A) {
            leftPressed = false;
        }
        if (code == KeyEvent.VK_D) {
            rightPressed = false;
        }
        if (code == KeyEvent.VK_ENTER) {
            enterPressed = false;
        }
        if (code == KeyEvent.VK_F) {
            shotKeyPressed = false;
        }
    }
}
